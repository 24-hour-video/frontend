import React, { Component } from 'react'
export default class ImageList extends Component {
  state = { imageList: [] }

  componentDidMount() {
    // fetch images
    fetch('https://nfsxvse81f.execute-api.eu-central-1.amazonaws.com/dev/images', {
      mode: "cors",
      credentials: "omit",
    })
      .then(response => response.json())
      .then(imagesResponse => {
        const imageList = imagesResponse.urls.map(image => ({ title: image.Key, src: imagesResponse.baseUrl + '/' + image.Key}))

        this.setState({
          imageList
        })
      })
      .catch(err => console.error(err))

  }

  render() {
    const images = this.state.imageList.map((image, key) => {
      console.log("hahah")
    return (
      <div key={key} className="card" style={{ 'width': '18rem'}}>
        <img className="card-img-top" src={image.src} alt={image.title}/>
        <div className="card-body">
            <h5 className="card-title">{image.title}</h5>
            <p className="card-text">{image.key}</p>
            <a href="#" className="btn btn-primary">Go somewhere</a>
        </div>
      </div>
      );
    });
    return <div>{images}</div>
  }
}